package app;

import java.util.ArrayList;
import java.util.List;

public class Warehouse {
    private List<Envelope> envelopes;
    private static volatile Warehouse warehouse;

    private Warehouse() {
        envelopes = new ArrayList<>();
    }

    public static Warehouse getInstance(){
        if(warehouse == null) {
            synchronized (Warehouse.class) {
                if (warehouse == null){
                    warehouse = new Warehouse();
                }
            }
        }
        return warehouse;
    }

    public synchronized boolean addEnvelope(Envelope envelope){
        if (envelopes.size() < 5 ) {
            envelopes.add(envelope);
            System.out.println(String.format("Envelope %s with weight %d added by thread # %s", envelope.getName(), envelope.getWeight(), Thread.currentThread().getName()));
            System.out.println("Envelopes in warehouse: " + envelopes.size());
            System.out.println();

            synchronized (this) {
                this.notifyAll();
            }

            return true;
        } else {
            System.out.println("Warehouse is full! Wait!");
            System.out.println();
            synchronized (this) {
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return false;
        }
    }

    public synchronized boolean removeEnvelope(){
        Envelope envelope = null;
        String type = ((Transport)Thread.currentThread()).getType().name();
        for (Envelope envp : envelopes) {
            if(Type.valueOf(type) == envp.getWeightType()){
                envelope = envp;
                break;
            }
        }

        if(envelope != null) {
            envelopes.remove(envelope);
            System.out.println(String.format("Envelope %s with weight %d removed by thread # %s", envelope.getName(), envelope.getWeight(), Thread.currentThread().getName()));
            System.out.println("Envelopes in envelopes: " + envelopes.size());
            System.out.println();
            synchronized (this) {
                this.notifyAll();
            }

            return true;
        } else {
            System.out.println("Not appropriate types or there is no such envelope for thread #" + Thread.currentThread().getName());
            System.out.println();
            return false;
        }

    }

    public int getSize(){
        return envelopes.size();
    }
}
