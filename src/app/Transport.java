package app;

/**
 * Created by bogdan.kulyk on 18-Nov-14.
 */
public class Transport extends Thread {
    private Warehouse warehouse;
    private Type type;
    private Envelope envelope;

    public Transport() {
    }

    public Transport(Type type, Envelope envelope, Warehouse warehouse) {
        this.type = type;
        this.envelope = envelope;
        this.warehouse = warehouse;
    }

    public void run(){
        System.out.println(String.format("Thread # %s was started", Thread.currentThread().getName()));
        while(!warehouse.addEnvelope(envelope)){

        }
        try {
            Thread.currentThread().sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        while(!warehouse.removeEnvelope()){}

    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Envelope getEnvelope() {
        return envelope;
    }

    public void setEnvelope(Envelope envelope) {
        this.envelope = envelope;
    }
}