package app;

/**
 * Created by bogdan.kulyk on 18-Nov-14.
 */
public class Envelope {
    private Type weightType;
    private int weight;
    private String name;

    public Envelope() {
    }

    public Envelope(Type weightType, int weight, String name) {
        this.weightType = weightType;
        this.weight = weight;
        this.name = name;
    }

    public Type getWeightType() {
        return weightType;
    }

    public void setWeightType(Type weightType) {
        this.weightType = weightType;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}