package app;

/**
 * Created by bogdan.kulyk on 18-Nov-14.
 */
public enum Type {
    SMALL,
    MIDDLE,
    BIG,
}