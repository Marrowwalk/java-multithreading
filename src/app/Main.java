package app;

import java.util.List;
import java.util.Random;

/**
 * Created by bogdan.kulyk on 18-Nov-14.
 */
public class Main {

    public static void main(String[] args) {
        Warehouse warehouse = Warehouse.getInstance();

        Envelope envp;

        for(int i = 0; i < 20; i++){
            Random rnd = new Random();

            int num = rnd.nextInt(3);
            switch (num) {
                case 0:
                    envp = new Envelope(Type.SMALL, 3, "small");
                    new Transport(Type.SMALL, envp, warehouse).start();
                    break;
                case 1:
                    envp = new Envelope(Type.MIDDLE, 500, "middle");
                    new Transport(Type.MIDDLE, envp, warehouse).start();
                    break;
                case 2:
                    envp = new Envelope(Type.BIG, 15000, "big");
                    new Transport(Type.BIG, envp, warehouse).start();
                    break;
                default:
                    System.out.println("Something wrong!");
            }
        }
    }
}
